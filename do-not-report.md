# This contains a list of bugs that will not be addressed.

- Duplicate friends on friends list.

- Anything pertaining to the legacy client (unless it poses a security risk)

- Bugs not seen on a major browser (Chrome, Safari, Firefox, Edge)

- node-hill bugs, do that on the node-hill discord.

- ALL clan bugs. (unless they pose a security risk)

- Broken item meshes.

- Unicode characters that cause issues (unclickable threads, etc).

- Bugs that were already patched / addressed. (ie. Bugs pertaining to the old site)

- Broken avatar renders.

- Multiple bugs in one issue (unless they are relevant to each other)

- Mobile bugs caused by using desktop mode on your browser app.

- Avatar still wearing item after trading / selling it.