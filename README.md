# bug-tracker

Welcome to the Brick Hill bug-tracker! This is a repo where you can submit issue reports
containing bugs on the website.

This poses as a fast and efficient way for you to report directly to the site developers
so we can patch your bugs asap.

## Reporting bugs
When reporting a bug, first make sure It's not on the "do-not-report" list.
You should read through that [here](do-not-report.md).

You can report bugs by creating a new issue [here](https://gitlab.com/brickhill/open-source/bug-tracker/issues).

__Be sure to follow the template that is auto-filled when creating an issue.__

If the bug is a vulnerability, or major enough that it could be abused. You are required 
to mark the issue as "confidential" (there's a checkbox for doing this before submitting.)

We take our user security very seriously, and will aim to fix security issues in under one business day.

Do **NOT** share vulnerabilities / major bugs with anyone, this could lead to your account being banned.